
class GameField {
    state = [
        [null, null, null],
        [null, null, null],
        [null, null, null]
        ];
    mode = 'x';
    isOverGame = false;
    turnCount = 0;
    oWin = false;
    xWin = false;

    getGameFieldStatus() {
        let all_rows = ' '
        for (let i = 0; i < 3; i ++) {
            for (let j = 0; j < 3; j ++) {
                if (gameField.state[i][j] == null) {
                    all_rows += '[  .  ]      ';
                } 
                else {
                    all_rows += "[ " + gameField.state[i][j] + " ]" + '      ';
                }
            }
            all_rows += '\n '
        }
        return all_rows;
    }

    setMode() {
        if (this.mode == 'x') {
            this.mode = 'o';
        }
        else {
            this.mode = 'x';
        }
    }

    fieldCellValue(x, y) {
        if ((x <= 2) && (x >= 0) && (y <= 2) && (y >= 0) && this.state[y][x] == null) {
            if (this.mode == 'x') {
                this.state[y][x] = 'x';
                this.turnCount ++;
                this.setMode();
            }
            else {
                this.state[y][x] = 'o';
                this.turnCount ++;
                this.setMode();
            }
        }

        else {
            alert("Введены неверные координаты! Попробуйте еще раз!")
        }

    }
    // Используется в след функции - проверка кто победил(из прошлого задания)
    winCheck (i,j) {
        if (gameField.state[i][j] == 'x') {
            this.xWin = true;
            return;
        }
        if (gameField.state[i][j] == 'o') {
            this.oWin = true;
            return;
        }
    }

    // Логика проверки поля из прошлого задания
    xologicCheck () {

        for (let i = 0; i < 3; i ++) {
            for (let j = 0; j < 3; j ++) {
                //lines
                if (j == 1)  {
                    if (this.state[i][j-1] == this.state[i][j] & this.state[i][j-1] == this.state[i][j+1]) {
                            this.winCheck(i,j);
                        }
                }
                //rows
                if (i == 1)  {
                    if (this.state[i-1][j] == this.state[i][j] & this.state[i-1][j] == this.state[i+1][j]) {
                            this.winCheck(i,j);
                        }
                
                if (i == 1 & j == 1) {
                    //diagonal1 00 11 22
                    if (this.state[i-1][j-1] == this.state[i][j] & this.state[i-1][j-1] == this.state[i+1][j+1]) {
                            this.winCheck(i,j) 
                        }
    
                    //diagonal 02 11 20
                    if (this.state[i][j] == this.state[i-1][j+1] & this.state[i][j] == this.state[i+1][j-1]) {
                            this.winCheck(i,j) 
                        }
                    } 
                }
            }
    
        
        
        }
        
        if (this.oWin == true) {
            alert('Нолики победили!\n\n             ПОЛЕ \n' + gameField.getGameFieldStatus() + 
            '\n\nОбновите сайт, чтобы начать сначала')
            this.isOverGame = true;
        }
        else if (this.xWin == true) {
            alert('Крестики победили!\n\n             ПОЛЕ \n' + gameField.getGameFieldStatus() + 
            '\n\nОбновите сайт, чтобы начать сначала')
            this.isOverGame = true;
        }
        else if (this.turnCount == 9) {
            alert('Ничья!\n\n             ПОЛЕ \n' + gameField.getGameFieldStatus() + 
            '\n\nОбновите сайт, чтобы начать сначала')
            this.isOverGame = true;
        }
    }

}

const gameField = new GameField();

alert('Начинаем игру! Первые ходят крестики!');

while (!gameField.isOverGame) {
    let xy = prompt('Ходит ' + gameField.mode.toUpperCase()  + '!' +
    ' Введите координаты через пробел!\n'+
    '(столбец, строка, x - (1-3), y - (1-3))\n\n             ПОЛЕ \n' + gameField.getGameFieldStatus())
    let cxy = xy.split(' ');
    gameField.fieldCellValue(cxy[0] - 1, cxy[1] - 1);
    gameField.getGameFieldStatus();
    gameField.xologicCheck();
}