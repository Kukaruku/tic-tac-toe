const fs = require('fs')
const bcrypt = require('bcrypt')

var incorrect = false;

module.exports.getPage = getPage = (req, res) => {
    res.render('../views/pages/auth.ejs', {
        incorrect: incorrect
    }) 
}

module.exports.getLP = getLP = (req, res) => {
    
    let readFile = fs.readFileSync('user.json', 'utf8');
    let parsedData = JSON.parse(readFile)

    let finded = false;
    for (let i = 0; i < parsedData.length; i++) {
        
        
        if (parsedData[i].login == req.body.login) {
            
            finded = true;

            if (bcrypt.compareSync(req.body.pass, parsedData[i].pass)) {
                res.redirect('/rating')
                incorrect = false;
                return;
            }
            
            else {
                incorrect = true;
                res.redirect('/login')
            }

        }

    }
    if (!finded) {
        incorrect = true;
        res.redirect('/login');
    }
}

