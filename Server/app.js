var express = require('express');
var path = require('path');
// var indexRouter = require('./routes/index');
var loginRouter = require('./routes/login');
var bcrypt = require('bcrypt');
var fs = require('fs')


const app = express();
const port = (process.env.PORT || 3000);

app.set('view engine', 'ejs');

app.use(express.json())
app.use(express.urlencoded())
app.use(express.static(path.resolve(__dirname, '../', 'Public', 'Auth page')));
app.use(express.static(path.resolve(__dirname, '../', 'Public', 'Main')));

app.get('/', (req, res) => {
    res.redirect('/login')
})

app.get('/rating', (req, res) => {
    let readFile = fs.readFileSync('ratings.json', 'utf8');
    let parsedData = JSON.parse(readFile)
    
    var ratings = parsedData;
    res.render('pages/rating', {
        ratings: ratings
    })
})

// app.use('/', indexRouter);
app.use(loginRouter);

// const saltRounds = 10;
// const myPlaintextPassword = '1243';
// bcrypt.genSalt(saltRounds, function(err, salt) {
//     bcrypt.hash(myPlaintextPassword, salt, function(err, hash) {
//         console.log(hash)
//     });
// });


app.listen(port, () => {
    console.log(`listening app on PORT ${port}`)
})



// var app = require('../app');
// var debug = require('debug')()


// var port = normalizePort(process.env.PORT || 3000);
// app.set('port', port);


// var server = http.createServer(app);

// server.listen(port);
// server.on('error', onError);
// server.on('listening', onListening);