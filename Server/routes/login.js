const { Router } = require("express");
const get = require('../controllers/login');
const router = Router()

router.get('/login', get.getPage)

router.post('/login', get.getLP)

// router.post('/', create)

module.exports = router;