class GameField {
    state = [
        [null, null, null],
        [null, null, null],
        [null, null, null]
        ];
    mode = 'x';
    isOverGame = false;
    turnCount = 0;
    oWin = false;
    xWin = false;
    firstPlayer = 'Вячеслав Екатерина';
    secondPlayer = 'Владелен Пупкин';

    getGameFieldStatus() {
        let all_rows = ' '
        for (let i = 0; i < 3; i ++) {
            for (let j = 0; j < 3; j ++) {
                if (gameField.state[i][j] == null) {
                    all_rows += '[ . ]      ';
                } 
                else {
                    all_rows += "[ " + gameField.state[i][j] + " ]" + '      ';
                }
            }
            all_rows += '\n '
        }
        console.log(all_rows);
    }

    setMode() {
        if (this.mode == 'x') {
            nameTurn.textContent = gameField.secondPlayer;
            imgTurn.classList.remove('x')
            imgTurn.classList.add('zero')
            this.mode = 'o';
        }
        else {
            nameTurn.textContent = gameField.firstPlayer;
            imgTurn.classList.remove('zero');
            imgTurn.classList.add('x');
            this.mode = 'x';
        }
    }

    fieldCellValue(x, y, element) {
        if (this.state[y][x] == null) {
            if (this.mode == 'x') {
                element.classList.add('x');
                this.state[y][x] = 'x';
                this.turnCount ++;
                this.setMode();
            }
            else {
                element.classList.add('zero');
                this.state[y][x] = 'o';
                this.turnCount ++;
                this.setMode();
            }
        }


    }
    // Используется в след методе - проверка кто победил(из прошлого задания)
    winCheck (i,j, comboArr) {
        if (gameField.state[i][j] == 'x') {
            comboArr.forEach((element) => {
                console.log(element)
                document.getElementById(element).classList.add('xWin')
            })
            this.xWin = true;
            return;
        }
        if (gameField.state[i][j] == 'o') {
            comboArr.forEach((element) => {
                console.log(element)
                document.getElementById(element).classList.add('oWin')
            })
            this.oWin = true;
            return;
        }
    }

    // Логика проверки поля из прошлого задания
    xologicCheck () {

        for (let i = 0; i < 3; i ++) {
            for (let j = 0; j < 3; j ++) {
                //lines
                if (j == 1)  {
                    if (this.state[i][j-1] == this.state[i][j] & this.state[i][j-1] == this.state[i][j+1] &
                        this.state[i][j] != null) {
                            let comboArr = [(j-1) + ' ' + i, j + ' ' + i, (j+1) + ' ' + i]
                            this.winCheck(i,j, comboArr);
                        }
                }
                //rows
                if (i == 1)  {
                    if (this.state[i-1][j] == this.state[i][j] & this.state[i-1][j] == this.state[i+1][j] &
                        this.state[i][j] != null) {
                            let comboArr = [j + ' ' + (i-1), j + ' ' + i, j + ' ' + (i+1)]
                            this.winCheck(i,j, comboArr);
                        }
                
                if (i == 1 & j == 1) {
                    //diagonal1 00 11 22
                    if (this.state[i-1][j-1] == this.state[i][j] & this.state[i-1][j-1] == this.state[i+1][j+1] &
                        this.state[i][j] != null) {
                            let comboArr = [(j-1) + ' ' + (i-1), j + ' ' + i, (j+1) + ' ' + (i+1)]
                            this.winCheck(i,j, comboArr) 
                        }
    
                    //diagonal 02 11 20
                    if (this.state[i][j] == this.state[i-1][j+1] & this.state[i][j] == this.state[i+1][j-1] &
                        this.state[i][j] != null) {
                            let comboArr = [(j+1) + ' ' + (i-1), (j + ' ' + i), (j-1 + ' ' + (i+1))]
                            this.winCheck(i,j, comboArr) 
                        }
                    } 
                }
            }
    
        
        
        }
        
        if (this.oWin) {
            document.querySelector('.winnerName').textContent = this.secondPlayer + ' победил(а)!';
            this.isOverGame = true;
        }
        else if (this.xWin) {
            document.querySelector('.winnerName').textContent = this.firstPlayer + ' победил(а)!';
            this.isOverGame = true;
        }
        else if (this.turnCount == 9) {
            document.querySelector('.winnerName').textContent = 'Ничья!';
            this.isOverGame = true;
        }
    }

}


const gameField = new GameField();


function addImg(e) {
    if (gameField.mode === 'x') {
        let xy = e.target.id;
        let cxy = xy.split(' ');
        gameField.fieldCellValue(cxy[0], cxy[1], e.target);
        gameField.getGameFieldStatus();
        gameField.xologicCheck(e.target);
        if (gameField.isOverGame) {
            allCells.forEach((element) => {
                element.removeEventListener('click', addImg);
            });
            document.querySelector('.endModal').style.display = 'block';
        }

    }
    else {
        let xy = e.target.id;
        let cxy = xy.split(' ');
        gameField.fieldCellValue(cxy[0], cxy[1], e.target);
        gameField.getGameFieldStatus();
        gameField.xologicCheck();
        if (gameField.isOverGame) {
            allCells.forEach((element) => {
                element.removeEventListener('click', addImg);
            });
            document.querySelector('.endModal').style.display = 'block';
        }
    }
}

// let cell = document.getElementById('cell1')
// cell.addEventListener('click', addImg)

let allCells = document.querySelectorAll('.cell')
let imgTurn = document.querySelector('.imgTurn')
let nameTurn = document.querySelector('.nameTurn')

document.getElementById('newGame').addEventListener('click', function newG() {
    location.reload();
})

allCells.forEach((element) => {
    element.addEventListener('click', addImg);
});

